import 'dart:ui' as ui;

import 'package:flutter/material.dart';

class LeftArrow extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.4510400, size.height * 0.4999967);
    path_0.lineTo(size.width * 0.6572933, size.height * 0.7062467);
    path_0.lineTo(size.width * 0.5983767, size.height * 0.7651633);
    path_0.lineTo(size.width * 0.3332083, size.height * 0.4999967);
    path_0.lineTo(size.width * 0.5983767, size.height * 0.2348307);
    path_0.lineTo(size.width * 0.6572933, size.height * 0.2937473);
    path_0.lineTo(size.width * 0.4510400, size.height * 0.4999967);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Color(0xff484848).withOpacity(1.0);
    canvas.drawPath(path_0, paint_0_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
