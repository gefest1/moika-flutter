import 'dart:ui' as ui;

import 'package:flutter/material.dart';

class SettingsIcon extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.shader = ui.Gradient.linear(
        Offset(size.width * -5.447406e-7, size.height * 0.4814825),
        Offset(size.width, size.height * 0.4814825), [
      Colors.white.withOpacity(1),
      Colors.white.withOpacity(1),
      Colors.white.withOpacity(1),
      Colors.white.withOpacity(0.517767),
      Colors.white.withOpacity(0)
    ], [
      0,
      0.531772,
      0.794792,
      0.89375,
      1
    ]);
    canvas.drawRect(Rect.fromLTWH(0, 0, size.width, size.height), paint_0_fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.3669344, size.height * 0.6937375);
    path_1.cubicTo(
        size.width * 0.3713719,
        size.height * 0.6736100,
        size.width * 0.3796031,
        size.height * 0.6561800,
        size.width * 0.3904937,
        size.height * 0.6438500);
    path_1.cubicTo(
        size.width * 0.4013828,
        size.height * 0.6315225,
        size.width * 0.4143938,
        size.height * 0.6249025,
        size.width * 0.4277344,
        size.height * 0.6249025);
    path_1.cubicTo(
        size.width * 0.4410750,
        size.height * 0.6249025,
        size.width * 0.4540859,
        size.height * 0.6315225,
        size.width * 0.4649750,
        size.height * 0.6438500);
    path_1.cubicTo(
        size.width * 0.4758656,
        size.height * 0.6561800,
        size.width * 0.4840969,
        size.height * 0.6736100,
        size.width * 0.4885344,
        size.height * 0.6937375);
    path_1.lineTo(size.width * 0.7070313, size.height * 0.6937375);
    path_1.lineTo(size.width * 0.7070313, size.height * 0.7624875);
    path_1.lineTo(size.width * 0.4885344, size.height * 0.7624875);
    path_1.cubicTo(
        size.width * 0.4840969,
        size.height * 0.7826150,
        size.width * 0.4758656,
        size.height * 0.8000425,
        size.width * 0.4649750,
        size.height * 0.8123725);
    path_1.cubicTo(
        size.width * 0.4540859,
        size.height * 0.8247000,
        size.width * 0.4410750,
        size.height * 0.8313200,
        size.width * 0.4277344,
        size.height * 0.8313200);
    path_1.cubicTo(
        size.width * 0.4143938,
        size.height * 0.8313200,
        size.width * 0.4013828,
        size.height * 0.8247000,
        size.width * 0.3904937,
        size.height * 0.8123725);
    path_1.cubicTo(
        size.width * 0.3796031,
        size.height * 0.8000425,
        size.width * 0.3713719,
        size.height * 0.7826150,
        size.width * 0.3669344,
        size.height * 0.7624875);
    path_1.lineTo(size.width * 0.2773438, size.height * 0.7624875);
    path_1.lineTo(size.width * 0.2773438, size.height * 0.6937375);
    path_1.lineTo(size.width * 0.3669344, size.height * 0.6937375);
    path_1.close();
    path_1.moveTo(size.width * 0.4958406, size.height * 0.4531125);
    path_1.cubicTo(
        size.width * 0.5002781,
        size.height * 0.4329850,
        size.width * 0.5085094,
        size.height * 0.4155550,
        size.width * 0.5194000,
        size.height * 0.4032250);
    path_1.cubicTo(
        size.width * 0.5302891,
        size.height * 0.3908975,
        size.width * 0.5433000,
        size.height * 0.3842775,
        size.width * 0.5566406,
        size.height * 0.3842775);
    path_1.cubicTo(
        size.width * 0.5699812,
        size.height * 0.3842775,
        size.width * 0.5829922,
        size.height * 0.3908975,
        size.width * 0.5938813,
        size.height * 0.4032250);
    path_1.cubicTo(
        size.width * 0.6047719,
        size.height * 0.4155550,
        size.width * 0.6130031,
        size.height * 0.4329850,
        size.width * 0.6174422,
        size.height * 0.4531125);
    path_1.lineTo(size.width * 0.7070313, size.height * 0.4531125);
    path_1.lineTo(size.width * 0.7070313, size.height * 0.5218625);
    path_1.lineTo(size.width * 0.6174422, size.height * 0.5218625);
    path_1.cubicTo(
        size.width * 0.6130031,
        size.height * 0.5419900,
        size.width * 0.6047719,
        size.height * 0.5594175,
        size.width * 0.5938813,
        size.height * 0.5717475);
    path_1.cubicTo(
        size.width * 0.5829922,
        size.height * 0.5840750,
        size.width * 0.5699812,
        size.height * 0.5906950,
        size.width * 0.5566406,
        size.height * 0.5906950);
    path_1.cubicTo(
        size.width * 0.5433000,
        size.height * 0.5906950,
        size.width * 0.5302891,
        size.height * 0.5840750,
        size.width * 0.5194000,
        size.height * 0.5717475);
    path_1.cubicTo(
        size.width * 0.5085094,
        size.height * 0.5594175,
        size.width * 0.5002781,
        size.height * 0.5419900,
        size.width * 0.4958406,
        size.height * 0.5218625);
    path_1.lineTo(size.width * 0.2773438, size.height * 0.5218625);
    path_1.lineTo(size.width * 0.2773438, size.height * 0.4531125);
    path_1.lineTo(size.width * 0.4958406, size.height * 0.4531125);
    path_1.close();
    path_1.moveTo(size.width * 0.3669344, size.height * 0.2124865);
    path_1.cubicTo(
        size.width * 0.3713719,
        size.height * 0.1923590,
        size.width * 0.3796031,
        size.height * 0.1749295,
        size.width * 0.3904937,
        size.height * 0.1626013);
    path_1.cubicTo(
        size.width * 0.4013828,
        size.height * 0.1502728,
        size.width * 0.4143938,
        size.height * 0.1436522,
        size.width * 0.4277344,
        size.height * 0.1436522);
    path_1.cubicTo(
        size.width * 0.4410750,
        size.height * 0.1436522,
        size.width * 0.4540859,
        size.height * 0.1502728,
        size.width * 0.4649750,
        size.height * 0.1626013);
    path_1.cubicTo(
        size.width * 0.4758656,
        size.height * 0.1749295,
        size.width * 0.4840969,
        size.height * 0.1923590,
        size.width * 0.4885344,
        size.height * 0.2124865);
    path_1.lineTo(size.width * 0.7070313, size.height * 0.2124865);
    path_1.lineTo(size.width * 0.7070313, size.height * 0.2812375);
    path_1.lineTo(size.width * 0.4885344, size.height * 0.2812375);
    path_1.cubicTo(
        size.width * 0.4840969,
        size.height * 0.3013650,
        size.width * 0.4758656,
        size.height * 0.3187925,
        size.width * 0.4649750,
        size.height * 0.3311225);
    path_1.cubicTo(
        size.width * 0.4540859,
        size.height * 0.3434500,
        size.width * 0.4410750,
        size.height * 0.3500700,
        size.width * 0.4277344,
        size.height * 0.3500700);
    path_1.cubicTo(
        size.width * 0.4143938,
        size.height * 0.3500700,
        size.width * 0.4013828,
        size.height * 0.3434500,
        size.width * 0.3904937,
        size.height * 0.3311225);
    path_1.cubicTo(
        size.width * 0.3796031,
        size.height * 0.3187925,
        size.width * 0.3713719,
        size.height * 0.3013650,
        size.width * 0.3669344,
        size.height * 0.2812375);
    path_1.lineTo(size.width * 0.2773438, size.height * 0.2812375);
    path_1.lineTo(size.width * 0.2773438, size.height * 0.2124865);
    path_1.lineTo(size.width * 0.3669344, size.height * 0.2124865);
    path_1.close();
    path_1.moveTo(size.width * 0.4277344, size.height * 0.2812375);
    path_1.cubicTo(
        size.width * 0.4334328,
        size.height * 0.2812375,
        size.width * 0.4388969,
        size.height * 0.2776150,
        size.width * 0.4429266,
        size.height * 0.2711675);
    path_1.cubicTo(
        size.width * 0.4469547,
        size.height * 0.2647225,
        size.width * 0.4492188,
        size.height * 0.2559775,
        size.width * 0.4492188,
        size.height * 0.2468615);
    path_1.cubicTo(
        size.width * 0.4492188,
        size.height * 0.2377448,
        size.width * 0.4469547,
        size.height * 0.2290012,
        size.width * 0.4429266,
        size.height * 0.2225547);
    path_1.cubicTo(
        size.width * 0.4388969,
        size.height * 0.2161083,
        size.width * 0.4334328,
        size.height * 0.2124865,
        size.width * 0.4277344,
        size.height * 0.2124865);
    path_1.cubicTo(
        size.width * 0.4220359,
        size.height * 0.2124865,
        size.width * 0.4165719,
        size.height * 0.2161083,
        size.width * 0.4125422,
        size.height * 0.2225547);
    path_1.cubicTo(
        size.width * 0.4085141,
        size.height * 0.2290012,
        size.width * 0.4062500,
        size.height * 0.2377448,
        size.width * 0.4062500,
        size.height * 0.2468615);
    path_1.cubicTo(
        size.width * 0.4062500,
        size.height * 0.2559775,
        size.width * 0.4085141,
        size.height * 0.2647225,
        size.width * 0.4125422,
        size.height * 0.2711675);
    path_1.cubicTo(
        size.width * 0.4165719,
        size.height * 0.2776150,
        size.width * 0.4220359,
        size.height * 0.2812375,
        size.width * 0.4277344,
        size.height * 0.2812375);
    path_1.close();
    path_1.moveTo(size.width * 0.5566406, size.height * 0.5218625);
    path_1.cubicTo(
        size.width * 0.5623391,
        size.height * 0.5218625,
        size.width * 0.5678031,
        size.height * 0.5182400,
        size.width * 0.5718328,
        size.height * 0.5117925);
    path_1.cubicTo(
        size.width * 0.5758609,
        size.height * 0.5053475,
        size.width * 0.5781250,
        size.height * 0.4966025,
        size.width * 0.5781250,
        size.height * 0.4874875);
    path_1.cubicTo(
        size.width * 0.5781250,
        size.height * 0.4783700,
        size.width * 0.5758609,
        size.height * 0.4696275,
        size.width * 0.5718328,
        size.height * 0.4631800);
    path_1.cubicTo(
        size.width * 0.5678031,
        size.height * 0.4567325,
        size.width * 0.5623391,
        size.height * 0.4531125,
        size.width * 0.5566406,
        size.height * 0.4531125);
    path_1.cubicTo(
        size.width * 0.5509422,
        size.height * 0.4531125,
        size.width * 0.5454781,
        size.height * 0.4567325,
        size.width * 0.5414484,
        size.height * 0.4631800);
    path_1.cubicTo(
        size.width * 0.5374203,
        size.height * 0.4696275,
        size.width * 0.5351563,
        size.height * 0.4783700,
        size.width * 0.5351563,
        size.height * 0.4874875);
    path_1.cubicTo(
        size.width * 0.5351563,
        size.height * 0.4966025,
        size.width * 0.5374203,
        size.height * 0.5053475,
        size.width * 0.5414484,
        size.height * 0.5117925);
    path_1.cubicTo(
        size.width * 0.5454781,
        size.height * 0.5182400,
        size.width * 0.5509422,
        size.height * 0.5218625,
        size.width * 0.5566406,
        size.height * 0.5218625);
    path_1.close();
    path_1.moveTo(size.width * 0.4277344, size.height * 0.7624875);
    path_1.cubicTo(
        size.width * 0.4334328,
        size.height * 0.7624875,
        size.width * 0.4388969,
        size.height * 0.7588650,
        size.width * 0.4429266,
        size.height * 0.7524175);
    path_1.cubicTo(
        size.width * 0.4469547,
        size.height * 0.7459725,
        size.width * 0.4492188,
        size.height * 0.7372275,
        size.width * 0.4492188,
        size.height * 0.7281125);
    path_1.cubicTo(
        size.width * 0.4492188,
        size.height * 0.7189950,
        size.width * 0.4469547,
        size.height * 0.7102525,
        size.width * 0.4429266,
        size.height * 0.7038050);
    path_1.cubicTo(
        size.width * 0.4388969,
        size.height * 0.6973575,
        size.width * 0.4334328,
        size.height * 0.6937375,
        size.width * 0.4277344,
        size.height * 0.6937375);
    path_1.cubicTo(
        size.width * 0.4220359,
        size.height * 0.6937375,
        size.width * 0.4165719,
        size.height * 0.6973575,
        size.width * 0.4125422,
        size.height * 0.7038050);
    path_1.cubicTo(
        size.width * 0.4085141,
        size.height * 0.7102525,
        size.width * 0.4062500,
        size.height * 0.7189950,
        size.width * 0.4062500,
        size.height * 0.7281125);
    path_1.cubicTo(
        size.width * 0.4062500,
        size.height * 0.7372275,
        size.width * 0.4085141,
        size.height * 0.7459725,
        size.width * 0.4125422,
        size.height * 0.7524175);
    path_1.cubicTo(
        size.width * 0.4165719,
        size.height * 0.7588650,
        size.width * 0.4220359,
        size.height * 0.7624875,
        size.width * 0.4277344,
        size.height * 0.7624875);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Color(0xff484848).withOpacity(1.0);
    canvas.drawPath(path_1, paint_1_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
