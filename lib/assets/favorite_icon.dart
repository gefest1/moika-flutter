import 'dart:ui' as ui;

import 'package:flutter/material.dart';

class FavoriteIcon extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.7188119, size.height * 0.003289474);
    path_0.cubicTo(
        size.width * 0.8664929,
        size.height * 0.003289474,
        size.width * 0.9861738,
        size.height * 0.1376097,
        size.width * 0.9861738,
        size.height * 0.3256579);
    path_0.cubicTo(
        size.width * 0.9861738,
        size.height * 0.7017553,
        size.width * 0.6215905,
        size.height * 0.9166658,
        size.width * 0.5000619,
        size.height * 0.9972579);
    path_0.cubicTo(
        size.width * 0.3785333,
        size.height * 0.9166658,
        size.width * 0.01395090,
        size.height * 0.7017553,
        size.width * 0.01395090,
        size.height * 0.3256579);
    path_0.cubicTo(
        size.width * 0.01395090,
        size.height * 0.1376097,
        size.width * 0.1354786,
        size.height * 0.003289474,
        size.width * 0.2813119,
        size.height * 0.003289474);
    path_0.cubicTo(
        size.width * 0.3717286,
        size.height * 0.003289474,
        size.width * 0.4514500,
        size.height * 0.05701763,
        size.width * 0.5000619,
        size.height * 0.1107455);
    path_0.cubicTo(
        size.width * 0.5486738,
        size.height * 0.05701763,
        size.width * 0.6283952,
        size.height * 0.003289474,
        size.width * 0.7188119,
        size.height * 0.003289474);
    path_0.close();
    path_0.moveTo(size.width * 0.5454643, size.height * 0.8416632);
    path_0.cubicTo(
        size.width * 0.5882905,
        size.height * 0.8117895,
        size.width * 0.6269381,
        size.height * 0.7820789,
        size.width * 0.6631048,
        size.height * 0.7502711);
    path_0.cubicTo(
        size.width * 0.8080143,
        size.height * 0.6229342,
        size.width * 0.8889500,
        size.height * 0.4837789,
        size.width * 0.8889500,
        size.height * 0.3256579);
    path_0.cubicTo(
        size.width * 0.8889500,
        size.height * 0.1988597,
        size.width * 0.8142357,
        size.height * 0.1107455,
        size.width * 0.7188119,
        size.height * 0.1107455);
    path_0.cubicTo(
        size.width * 0.6665071,
        size.height * 0.1107455,
        size.width * 0.6099238,
        size.height * 0.1413705,
        size.width * 0.5687976,
        size.height * 0.1867171);
    path_0.lineTo(size.width * 0.5000619, size.height * 0.2626887);
    path_0.lineTo(size.width * 0.4313262, size.height * 0.1867171);
    path_0.cubicTo(
        size.width * 0.3902000,
        size.height * 0.1413705,
        size.width * 0.3336167,
        size.height * 0.1107455,
        size.width * 0.2813119,
        size.height * 0.1107455);
    path_0.cubicTo(
        size.width * 0.1870064,
        size.height * 0.1107455,
        size.width * 0.1111731,
        size.height * 0.1997192,
        size.width * 0.1111731,
        size.height * 0.3256579);
    path_0.cubicTo(
        size.width * 0.1111731,
        size.height * 0.4838342,
        size.width * 0.1921593,
        size.height * 0.6229342,
        size.width * 0.3369714,
        size.height * 0.7502711);
    path_0.cubicTo(
        size.width * 0.3731881,
        size.height * 0.7820789,
        size.width * 0.4118333,
        size.height * 0.8117895,
        size.width * 0.4546595,
        size.height * 0.8416079);
    path_0.cubicTo(
        size.width * 0.4691929,
        size.height * 0.8517632,
        size.width * 0.4835833,
        size.height * 0.8614868,
        size.width * 0.5000619,
        size.height * 0.8723421);
    path_0.cubicTo(
        size.width * 0.5165405,
        size.height * 0.8614868,
        size.width * 0.5309310,
        size.height * 0.8517632,
        size.width * 0.5454643,
        size.height * 0.8416632);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Color(0xffFF0000).withOpacity(1.0);
    canvas.drawPath(path_0, paint_0_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
