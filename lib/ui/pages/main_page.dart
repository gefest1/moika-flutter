import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:moika/assets/favorite_icon.dart';
import 'package:moika/assets/setting_icon.dart';
import 'package:moika/ui/atoms/big_card.dart';

class MainPage extends StatefulWidget {
  MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            color: Colors.purple,
            child: Center(child: Text('MAP MAP MAP MAP MAP ')),
          ),
          CustomScrollView(
            physics: NeverScrollableScrollPhysics(),
            slivers: [
              SliverToBoxAdapter(
                child: Container(
                  height: 74,
                  color: Colors.white,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30,
                      ),
                      Row(
                        // crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          CustomPaint(
                            painter: SettingsIcon(),
                            size: Size(64, 40),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                child: Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text('Открыто'),
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.transparent,
                                    border: Border.all(
                                      color: Color(0xFF484848),
                                    )),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Container(
                                child: Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text('Свободно'),
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.transparent,
                                    border: Border.all(
                                      color: Color(0xFF484848),
                                    )),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Container(
                                child: Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text('Круглосуточно'),
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.transparent,
                                    border: Border.all(
                                      color: Color(0xFF484848),
                                    )),
                              )
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          DraggableScrollableSheet(
            initialChildSize: 0.30,
            minChildSize: 0.15,
            maxChildSize: 0.7,
            snap: true,
            builder: (BuildContext context, ScrollController scrollController) {
              return SingleChildScrollView(
                controller: scrollController,
                child: Container(
                  height: 800,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.white,
                  ),
                  child: Scaffold(
                    body: Column(
                      children: [
                        SizedBox(
                          height: 15,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Center(
                              child: Text('Все автомойки: 150'),
                            ),
                          ],
                        ),
                        SizedBox(height: 20),
                        Container(
                          height: 500,
                          child: ListView(
                            padding: EdgeInsets.all(0),
                            children: [
                              SizedBox(height: 10),
                              BigCard(),
                              SizedBox(height: 30),
                              Divider(),
                              SizedBox(height: 30),
                              BigCard(),
                              SizedBox(height: 30),
                              Divider(),
                              SizedBox(height: 30),
                              BigCard(),
                              SizedBox(height: 30),
                              Divider(),
                              SizedBox(height: 30),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
