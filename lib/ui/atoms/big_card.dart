import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:moika/assets/favorite_icon.dart';

class BigCard extends StatefulWidget {
  BigCard({Key? key}) : super(key: key);

  @override
  State<BigCard> createState() => _BigCardState();
}

class _BigCardState extends State<BigCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Супер-пупер мойка с длинным названием',
              style: TextStyle(fontSize: 28, color: Color(0xFF0070F3)),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Шагабутдинова, 45. \nУгол улицы Мынбаева 1005005'),
                Text('4,8 км')
              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    RatingBar.builder(
                      initialRating: 0,
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemSize: 20,
                      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {
                        print(rating);
                      },
                    ),
                    Text('4,8'),
                  ],
                ),
                Text('Отзывов: 5')
              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Время работы',
                        style: TextStyle(
                          color: Color(0xFF9B9B9B),
                        ),
                      ),
                      Text(
                        '8:00 — 24:00',
                        style: TextStyle(
                          color: Color(0xFF0EB200),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Цены за услуги',
                        style: TextStyle(
                          color: Color(0xFF9B9B9B),
                        ),
                      ),
                      Text('от 1500 тг.'),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Мест сегодня',
                        style: TextStyle(
                          color: Color(0xFF9B9B9B),
                        ),
                      ),
                      Text('15'),
                    ],
                  ),
                )
              ],
            ),
            SizedBox(height: 10),
            Row(
              children: [
                Expanded(
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Color(0xFFFF0000),
                        ),
                        borderRadius: BorderRadius.circular(15)),
                    child: Center(
                      child: Text(
                        'Забронировать',
                        style: TextStyle(
                          color: Color(0xFFFF0000),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 10),
                CustomPaint(
                  painter: FavoriteIcon(),
                  size: Size(40, 40),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
